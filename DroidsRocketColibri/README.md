# RocketColibri remote control system


The RocketColibri application runs on a tablet / smartphone running Android >=4.1.

The application controls servos conneted to a Rasperry Pi running the ServoController service.

[https://bitbucket.org/lschelling/servocontroller](https://bitbucket.org/lschelling/servocontroller)

## Development

Currently the project is developed using the Eclipse IDE with the ADT plugin.

I am running Eclipse version 2022/06 with openjdk 11.

Since newer Eclipse version are running with openjdk > 8 some tweaks are required to get the ADT running:

### Missing BASE64Encoder/Decoder required by the `Android Package Builder`

The `Android Package Builder` builder is based on an older JDK and uses the BASE64Encoder classes which are not available anymore in Java > 9 version.

To fix the issu go into Java 8 sdk fodler, from jre\lib\rt.jar copy to sdklib.jar (it is somewhere in eclipse folder) classes (with same paths):

```
sun/misc/BASE64Decoder.class,
sun/misc/BASE64Encoder.class,
sun/misc/CharacterDecoder.class,
sun/misc/CharacterEncoder.class
```

### Fix a Breaking Change in `dx.jar`

Eclipse ADT no more support. So Google break backward compatibility with remove two classes from `dx.jar`.

You can easy fix it.

- Go to your sdk folder. Navigate to `dx.jar` from latest build-tools.  For example `build-tools\28.0.3\lib`
Open `dx.jar` in any zip archiver.
- Navigate to path `com\android\dx\command` inside archive.
- Here you not see files `DxConsole$1.class` and `DxConsole.class`.
- Now navigate to `dx.jar` for `25.0.3` or before.
- Again navigate to `com\android\dx\command` inside this archive.
- Here you see files `DxConsole$1.class` and `DxConsole.class`.
- Copy it from old `dx.jar` to new `dx.jar`.

All done. Now you can use new dx.jar with Eclipse ADT.

This solution better from replace `dx.jar`, because you can use new version of the `dx.jar`.

