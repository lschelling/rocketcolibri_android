package ch.hsr.rocketcolibri.util;

import android.text.Editable;
import android.util.Log;
import android.text.TextWatcher;

public class CheckIntegerRange implements TextWatcher {
	int min=0;
	int max=0;
	
	public CheckIntegerRange (int min, int max)
	{
		this.min = min;
		this.max = max;
	}

	public void afterTextChanged(Editable s) 
	{
		try {
			Log.d("value", "input: " + s);
			if (s.toString().length() > 0)
			{
				if(Integer.parseInt(s.toString())>max)
					s.replace(0, s.length(), Integer.toString(max));
				if(Integer.parseInt(s.toString())<min)
					s.replace(0, s.length(), Integer.toString(min));
			}
		}
		catch(NumberFormatException nfe){}
	}
	 
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		          // Not used, details on text just before it changed
		          // used to track in detail changes made to text, e.g. implement an undo
	}
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		          // Not used, details on text at the point change made
	}
}

