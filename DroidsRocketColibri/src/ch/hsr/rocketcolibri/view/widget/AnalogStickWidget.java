package ch.hsr.rocketcolibri.view.widget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import ch.hsr.rocketcolibri.RCConstants;
import ch.hsr.rocketcolibri.protocol.RocketColibriProtocolFsm.s;
import ch.hsr.rocketcolibri.ui_data.input.IUiInputSource;
import ch.hsr.rocketcolibri.ui_data.input.UiInputData;
import ch.hsr.rocketcolibri.ui_data.input.UiInputSourceChannel;
import ch.hsr.rocketcolibri.ui_data.output.ConnectionState;
import ch.hsr.rocketcolibri.ui_data.output.IUiOutputSinkChangeObserver;
import ch.hsr.rocketcolibri.ui_data.output.UiOutputDataType;
import ch.hsr.rocketcolibri.util.DrawingTools;
import ch.hsr.rocketcolibri.view.AbsoluteLayout.LayoutParams;
import ch.hsr.rocketcolibri.view.custimizable.ICustomizableView;
import ch.hsr.rocketcolibri.view.custimizable.ModusChangeListener;
import ch.hsr.rocketcolibri.view.custimizable.ViewElementConfig;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

public class AnalogStickWidget extends View implements ICustomizableView, IUiInputSource, IUiOutputSinkChangeObserver {
	public static final int INVALID_POINTER_ID = -1;
	private boolean tDebug;
	private boolean tIsControlling = false;

	private Paint dbgPaint1;
	private Paint dbgLine;

	private Paint bgPaint;
	private Paint handleStick;
	private Paint widgetCoordinates;
	private Paint handlePaint;
	private RectF stickBackgroundRect;
	private Vibrator vibrator;
	private boolean vibratorIsActive = false;
	private int innerPadding;
	private int bgRadius;
	private int stickRadius;
	private int handleRadius;
	private int movementRadius;
	private int handleInnerBoundaries;

	// Last touch point in view coordinates
	private int pointerId = INVALID_POINTER_ID;
	private float touchX, touchY;
	// Handle center in view coordinates
	private float handleX, handleY;
	// Center of the view in view coordinates
	private int cX, cY;
	// Size of the view in view coordinates
	private int dimension;
	
	private UiInputSourceChannel tChannelV = new UiInputSourceChannel();
	private UiInputSourceChannel tChannelH = new UiInputSourceChannel();
	private boolean tCustomizeModusActive = false;
	protected RCWidgetConfig tWidgetConfig;
	protected OnTouchListener tCustomizeModusListener;
	private ControlModeListener tInternalControlListener = new ControlModeListener();
	private ModusChangeListener tModusChangeListener = new ModusChangeListener() {

	public void customizeModeDeactivated() {}
	public void customizeModeActivated() {}};

	public AnalogStickWidget(Context context, ViewElementConfig viewElementConfig) {
		this(context, new RCWidgetConfig(viewElementConfig));
	}
	
	public AnalogStickWidget(Context context, RCWidgetConfig rcWidgetConfig) {
		super(context);
		tWidgetConfig = rcWidgetConfig;
		setLayoutParams(rcWidgetConfig.viewElementConfig.getLayoutParams());
		setAlpha(tWidgetConfig.viewElementConfig.getAlpha());
		initJoystickView(context);
	}

	public AnalogStickWidget(Context context, AttributeSet attrs) {
		super(context, attrs);
		initJoystickView(context);
	}

	public AnalogStickWidget(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initJoystickView(context);
	}

	private void initJoystickView(Context context) {
		setFocusable(true);
		dbgPaint1 = new Paint(Paint.ANTI_ALIAS_FLAG);
		dbgPaint1.setColor(Color.RED);
		dbgPaint1.setStrokeWidth(1);
		dbgPaint1.setStyle(Paint.Style.STROKE);

		dbgLine = new Paint(Paint.ANTI_ALIAS_FLAG);
		dbgLine.setColor(Color.GREEN);
		dbgLine.setStrokeWidth(1);
		dbgLine.setStyle(Paint.Style.FILL_AND_STROKE);
		dbgLine.setTextSize(getPixels(15));

		bgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		bgPaint.setColor(Color.GRAY);
		bgPaint.setStrokeWidth(1);
		bgPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		
		handleStick = new Paint(Paint.ANTI_ALIAS_FLAG);
		handleStick.setStrokeWidth(20);
		handleStick.setStyle(Paint.Style.FILL_AND_STROKE);

		widgetCoordinates = new Paint(Paint.ANTI_ALIAS_FLAG);
		widgetCoordinates.setColor(Color.LTGRAY);
		widgetCoordinates.setStrokeWidth(5);
		widgetCoordinates.setStyle(Paint.Style.FILL_AND_STROKE);
		
		handlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		handlePaint.setColor(Color.parseColor("#4F4F4F"));
		handlePaint.setStrokeWidth(20);
		handlePaint.setStyle(Paint.Style.FILL_AND_STROKE);
		
		stickBackgroundRect = new RectF();
		innerPadding = 0;

		vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		initDefaultProtocolConfig();
		initListener();
	}
	
	private int getPixels(float size) {
	    DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
	    return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, size, metrics);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// Here we make sure that we have a perfect circle
		int measuredWidth = measure(widthMeasureSpec);
		int measuredHeight = measure(heightMeasureSpec);
		setMeasuredDimension(measuredWidth, measuredHeight);
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		super.onLayout(changed, left, top, right, bottom);

		calculateDimensions();
	}

	private void calculateDimensions() {
		int d = Math.min(getLayoutParams().width, getLayoutParams().height);
		dimension = d;
		
		cX = d / 2;
		cY = d / 2;
		bgRadius = dimension / 2-innerPadding;
		stickRadius = (int) (bgRadius-bgRadius*0.1);
		handleRadius = (int) (d * 0.13);
		handleStick.setStrokeWidth((float) (handleRadius * 0.75));
		handleInnerBoundaries = handleRadius;
		movementRadius = Math.min(cX, cY) - handleInnerBoundaries;

		tChannelV.setWidgetRange(movementRadius, -movementRadius);
		tChannelH.setWidgetRange(-movementRadius, movementRadius);
		touchX = tChannelH.setWidgetToDefault();
		touchY = tChannelV.setWidgetToDefault();
	}

	private int measure(int measureSpec) {
		int result = 0;
		// Decode the measurement specifications.
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);
		if (specMode == MeasureSpec.UNSPECIFIED) {
			// Return a default size of 200 if no bounds are specified.
			result = 200;
		} else {
			// As you want to fill the available space
			// always return the full available bounds.
			result = specSize;
		}
		return result;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.save();
		drawBackground(canvas);
		drawCoordinates(canvas);
		drawHandle(canvas);
		if (tDebug)
			drawDebugInfo(canvas);
		canvas.restore();
		if (tCustomizeModusActive) 
			DrawingTools.drawCustomizableForground(this, canvas);
	}

	private void drawDebugInfo(Canvas canvas) {

		canvas.drawCircle(handleX, handleY, 3, dbgPaint1);
		canvas.drawRect(cX - movementRadius, cY - movementRadius, cX + movementRadius, cY + movementRadius, dbgPaint1);
		// Origin to touch point
		canvas.drawLine(cX, cY, handleX, handleY, dbgLine);

		String dbgString;
		if (UiInputSourceChannel.CHANNEL_UNASSIGNED == tChannelH.getChannelAssignment() ){
			dbgString = String.format(Locale.getDefault(),"H:unassigned");
			dbgLine.setColor(Color.RED);
		}
		else {
			dbgString = String.format(Locale.getDefault(), "H[%d]:%d", tChannelH.getChannelAssignment(), tChannelH.getChannelValue());
			dbgLine.setColor(Color.GREEN);
		}
		canvas.drawText(dbgString,0, getHeight()/2, dbgLine);
		if (UiInputSourceChannel.CHANNEL_UNASSIGNED == tChannelV.getChannelAssignment() ){
			dbgString = String.format(Locale.getDefault(), "V:unassigned");
			dbgLine.setColor(Color.RED);
		}
		else{
			dbgString = String.format(Locale.getDefault(), "V[%d]:%d", tChannelV.getChannelAssignment(), tChannelV.getChannelValue());
			dbgLine.setColor(Color.GREEN);
		}
		canvas.drawText(dbgString,getWidth()/2, dbgLine.getTextSize(), dbgLine);
		dbgLine.setColor(Color.GREEN);
	}

	private void drawHandle(Canvas canvas) {
		// Draw the handle
		if ((UiInputSourceChannel.CHANNEL_UNASSIGNED == tChannelV.getChannelAssignment() ) &&
		    (UiInputSourceChannel.CHANNEL_UNASSIGNED == tChannelH.getChannelAssignment() ))
		{
			handleY = touchY + cY;
			handleX = touchX + cX;
		}
		else
		{
			if (UiInputSourceChannel.CHANNEL_UNASSIGNED != tChannelV.getChannelAssignment() )
				handleY = touchY + cY;
			else
				handleY = cY;
	
			if (UiInputSourceChannel.CHANNEL_UNASSIGNED != tChannelH.getChannelAssignment() )
				handleX = touchX + cX;
			else
				handleX = cX;
		}
		
		canvas.drawCircle(cX, cY, (handleStick.getStrokeWidth()/10), handleStick);
		canvas.drawLine(cX, cY, handleX, handleY, handleStick);
		handlePaint.setColor(tIsControlling ? Color.BLUE : Color.DKGRAY);
		canvas.drawCircle(handleX, handleY, handleRadius, handlePaint);
	}

	private void drawBackground(Canvas canvas) {
		bgPaint.setColor(Color.GRAY);
		float xmin, xmax, ymin, ymax ; 
		if ((UiInputSourceChannel.CHANNEL_UNASSIGNED == tChannelV.getChannelAssignment() ) &&
			(UiInputSourceChannel.CHANNEL_UNASSIGNED == tChannelH.getChannelAssignment() ))
		{
			ymin = cY-stickRadius;
			ymax = cY+stickRadius;
			xmin = cX-stickRadius;
			xmax = cX+stickRadius;
		}
		else
		{
			if (UiInputSourceChannel.CHANNEL_UNASSIGNED != tChannelV.getChannelAssignment() )
			{
				ymin = cY-stickRadius;
				ymax = cY+stickRadius;
			}
			else
			{
				ymin = cY-handleRadius;
				ymax = cY+handleRadius;
			}
			if (UiInputSourceChannel.CHANNEL_UNASSIGNED != tChannelH.getChannelAssignment() )
			{
				xmin = cX-stickRadius;
				xmax = cX+stickRadius;
			}
			else
			{
				xmin = cX-handleRadius;
				xmax = cX+handleRadius;
			}
		}
		stickBackgroundRect.set(xmin, ymin, xmax, ymax);
		canvas.drawRoundRect(stickBackgroundRect, stickRadius/2, stickRadius/2, bgPaint);
	}

	private void drawCoordinates(Canvas canvas) {
		if (UiInputSourceChannel.CHANNEL_UNASSIGNED != tChannelV.getChannelAssignment() )
		{
			canvas.drawLine(cX, cY - stickRadius,cX, cY + stickRadius, widgetCoordinates);
			for(int i=cY-stickRadius; i<cY+stickRadius; i+=stickRadius/4)
				canvas.drawLine(cX-10, i,cX+10, i, widgetCoordinates);
		}
		if (UiInputSourceChannel.CHANNEL_UNASSIGNED != tChannelH.getChannelAssignment() )
		{
			canvas.drawLine(cX - stickRadius, cY, cX + stickRadius, cY, widgetCoordinates);
			for(int i=cX-stickRadius; i<cX+stickRadius; i+=stickRadius/4)
				canvas.drawLine(i, cX-10, i,cX+10, widgetCoordinates);
		}
	}

	// Constrain touch within a box
	private boolean constrainBox() {
		touchX = Math.max(Math.min(touchX, movementRadius), -movementRadius);
		touchY = Math.max(Math.min(touchY, movementRadius), -movementRadius);
		return ((movementRadius==Math.abs(touchX)) || (movementRadius==Math.abs(touchY)));
		
	}

	private class ControlModeListener implements OnTouchListener{
		@Override
		public boolean onTouch(View v, MotionEvent ev) {

		final int action = ev.getAction();
		switch (action & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_MOVE: {
				return processMoveEvent(ev);
			}
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP: {
				if (pointerId != INVALID_POINTER_ID) {
					// Log.d(AnalogStickWidget.class.getSimpleName(), "ACTION_UP");
					returnHandleToInitialPosition();
					pointerId =INVALID_POINTER_ID;
				}
				break;
			}
			case MotionEvent.ACTION_POINTER_UP: {
				if (pointerId != INVALID_POINTER_ID) {
					final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
					final int pId = ev.getPointerId(pointerIndex);
					if (pId == pointerId) {
						// Log.d(AnalogStickWidget.class.getSimpleName(), "ACTION_POINTER_UP: " + pointerId);
						returnHandleToInitialPosition();
						pointerId = INVALID_POINTER_ID;
						return true;
					}
				}
				break;
			}
			case MotionEvent.ACTION_DOWN: {
				if (pointerId == INVALID_POINTER_ID) {
					int x = (int) ev.getX();
					if (x >= 0 && x <  dimension) {
						pointerId = ev.getPointerId(0);
						// Log.d(AnalogStickWidget.class.getSimpleName(), "ACTION_DOWN: " + getPointerId());
						return true;
					}
				}
				break;
			}
			case MotionEvent.ACTION_POINTER_DOWN: {
				if (pointerId == INVALID_POINTER_ID) {
					final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
					final int pId = ev.getPointerId(pointerIndex);
					int x = (int) ev.getX(pId);
					if (x >= 0 && x < dimension) {
						// Log.d(TAG, "ACTION_POINTER_DOWN: " + pId);
						pointerId= pId;
						return true;
					}
				}
				break;
			}
		}
		return false;
		}
	}

	private boolean processMoveEvent(MotionEvent ev) {
		if (pointerId != INVALID_POINTER_ID) {
			final int pointerIndex = ev.findPointerIndex(pointerId);
			// Translate touch position to center of view
			float x = ev.getX(pointerIndex);
			touchX = x - cX;
			float y = ev.getY(pointerIndex);
			touchY = y - cY;
			reportOnMoved();
			invalidate();
			return true;
		}
		return false;
	}

	private void reportOnMoved() {
		updateVibrator(constrainBox());
		calcUserCoordinates(); 
	}

	private void updateVibrator(boolean touchConstraint)
	{
		if(vibrator.hasVibrator())
		{
			if(touchConstraint)
			{
				if (!vibratorIsActive)
				{
					vibrator.vibrate(100);
					vibratorIsActive=true;
				}
			}
			else
			{
				if(vibratorIsActive)
				{
					vibratorIsActive=false;
					vibrator.cancel();
				}
			}
		}
	}
	
	private void calcUserCoordinates() {
		tChannelH.setWidgetPosition((int)touchX);
		tChannelV.setWidgetPosition((int)touchY);
	}

	private void returnHandleToInitialPosition() 
	{
		final int numberOfFrames = 2;
		int widgetDefX = tChannelH.setWidgetToDefault();
		int widgetDefY = tChannelV.setWidgetToDefault();
		final double intervalsX = tChannelH.getWidgetSticky() ? 0 :
			(widgetDefX - touchX) / numberOfFrames;
		
		final double intervalsY = tChannelV.getWidgetSticky() ? 0 :
			(widgetDefY - touchY) / numberOfFrames;
					
		for (int i = 0; i < numberOfFrames; i++) 
		{
			postDelayed(new Runnable() 
			{
				public void run() 
				{
					touchX += intervalsX;
					touchY += intervalsY;
					reportOnMoved();
					invalidate();
				}
			}, i * 40);
		}
	}

	public interface AnalogStickClickedListener {
		public void OnClicked();

		public void OnReleased();
	}
	
	private void initDefaultProtocolConfig(){
		if(tWidgetConfig.protocolMap==null){
			tWidgetConfig.protocolMap = new HashMap<String, String>();
			if (UiInputSourceChannel.CHANNEL_UNASSIGNED == tChannelH.getChannelAssignment() )
				tWidgetConfig.protocolMap.put(RCConstants.CHANNEL_ASSIGNMENT_H, "");
			else
				tWidgetConfig.protocolMap.put(RCConstants.CHANNEL_ASSIGNMENT_H, Integer.valueOf(tChannelH.getChannelAssignment()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.INVERTED_H, Boolean.valueOf(tChannelH.getChannelInverted()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.MAX_RANGE_H, Integer.valueOf(tChannelH.getChannelMaxRange()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.MIN_RANGE_H, Integer.valueOf(tChannelH.getChannelMinRange()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.DEFAULT_POSITION_H, Integer.valueOf(tChannelH.getChannelDefaultPosition()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.TRIMM_H, Integer.valueOf(tChannelH.getChannelTrimm()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.STICKY_H, Boolean.valueOf(tChannelH.getWidgetSticky()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.EXPO_H, Boolean.valueOf(tChannelH.getWidgetSticky()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.FAILSAFE_H, Integer.valueOf(tChannelH.getChannelFailsafePosition()).toString());

			if (UiInputSourceChannel.CHANNEL_UNASSIGNED == tChannelV.getChannelAssignment() )
				tWidgetConfig.protocolMap.put(RCConstants.CHANNEL_ASSIGNMENT_V, "");
			else
				tWidgetConfig.protocolMap.put(RCConstants.CHANNEL_ASSIGNMENT_V, Integer.valueOf(tChannelV.getChannelAssignment()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.INVERTED_V, Boolean.valueOf(tChannelV.getChannelInverted()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.MAX_RANGE_V, Integer.valueOf(tChannelV.getChannelMaxRange()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.MIN_RANGE_V, Integer.valueOf(tChannelV.getChannelMinRange()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.DEFAULT_POSITION_V, Integer.valueOf(tChannelV.getChannelDefaultPosition()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.TRIMM_V, Integer.valueOf(tChannelV.getChannelTrimm()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.STICKY_V, Boolean.valueOf(tChannelV.getWidgetSticky()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.EXPO_V, Boolean.valueOf(tChannelV.getWidgetSticky()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.FAILSAFE_V, Integer.valueOf(tChannelV.getChannelFailsafePosition()).toString());
			tWidgetConfig.protocolMap.put(RCConstants.DEBUG, Boolean.valueOf(false).toString());
		}else{
			updateProtocolMap();
		}
	}
	
	@Override
	public void updateProtocolMap() {
		try{
			tChannelH.setChannelAssignment(getProtocolMapInt(RCConstants.CHANNEL_ASSIGNMENT_H));
			tChannelH.setChannelInverted(getProtocolMapBoolean(RCConstants.INVERTED_H));
			tChannelH.setChannelMaxRange(getProtocolMapInt(RCConstants.MAX_RANGE_H));
			tChannelH.setChannelMinRange(getProtocolMapInt(RCConstants.MIN_RANGE_H));
			tChannelH.setChannelDefaultPosition(getProtocolMapInt(RCConstants.DEFAULT_POSITION_H));
			tChannelH.setChannelTrimm(getProtocolMapInt(RCConstants.TRIMM_H));
			tChannelH.setWidgetSticky(getProtocolMapBoolean(RCConstants.STICKY_H));
			tChannelH.setWidgetExpo(getProtocolMapBoolean(RCConstants.EXPO_H));
			tChannelH.setChannelFailsafePosition(getProtocolMapInt(RCConstants.FAILSAFE_H));
			
			tChannelV.setChannelAssignment(getProtocolMapInt(RCConstants.CHANNEL_ASSIGNMENT_V));
			tChannelV.setChannelInverted(getProtocolMapBoolean(RCConstants.INVERTED_V));
			tChannelV.setChannelMaxRange(getProtocolMapInt(RCConstants.MAX_RANGE_V));
			tChannelV.setChannelMinRange(getProtocolMapInt(RCConstants.MIN_RANGE_V));
			tChannelV.setChannelDefaultPosition(getProtocolMapInt(RCConstants.DEFAULT_POSITION_V));
			tChannelV.setChannelTrimm(getProtocolMapInt(RCConstants.TRIMM_V));
			tChannelV.setWidgetSticky(getProtocolMapBoolean(RCConstants.STICKY_V));
			tChannelV.setWidgetExpo(getProtocolMapBoolean(RCConstants.EXPO_V));
			tChannelV.setChannelFailsafePosition(getProtocolMapInt(RCConstants.FAILSAFE_V));
			tDebug = getProtocolMapBoolean(RCConstants.DEBUG);
			calculateDimensions();
			postInvalidate();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void initListener(){
		setOnTouchListener(tInternalControlListener);
		
		tModusChangeListener = new ModusChangeListener() {
			public void customizeModeDeactivated() {
				setOnTouchListener(tInternalControlListener); 
			}
			public void customizeModeActivated() {
				setOnTouchListener(tCustomizeModusListener);
			}
		};
	}
	
	
	@Override
	public void create(RCWidgetConfig rcWidgetConfig) {
		tWidgetConfig = rcWidgetConfig;
//		init(getContext(), null);
	}

	@Override
	public void create(ViewElementConfig vElementConfig) {
		tWidgetConfig = new RCWidgetConfig(vElementConfig);
//		init(getContext(), null);
	}

	@Override
	public RCWidgetConfig getWidgetConfig() {
		tWidgetConfig.viewElementConfig = this.getViewElementConfig();
		return tWidgetConfig;
	}

	@Override
	public Map<String, String> getProtocolMap() {
		return tWidgetConfig.protocolMap;
	}

	protected int getProtocolMapInt(String key){
		try{
			return Integer.parseInt(tWidgetConfig.protocolMap.get(key));
		}catch(NumberFormatException e){
			return -1;
		}
	}
	
	protected boolean getProtocolMapBoolean(String key){
		try{
			return Boolean.parseBoolean(tWidgetConfig.protocolMap.get(key));
		}catch(Exception e){
			return false;
		}
	}

	@Override
	public void setCustomizeModus(boolean enabled) {
		if (tCustomizeModusActive != enabled) {
			if (enabled) {
				tModusChangeListener.customizeModeActivated();
			} else {
				tModusChangeListener.customizeModeDeactivated();
			}
			invalidate();
			tCustomizeModusActive = enabled;
		}
	}

	@Override
	public ViewElementConfig getViewElementConfig() {
		tWidgetConfig.viewElementConfig.setLayoutParams((LayoutParams) getLayoutParams());
		tWidgetConfig.viewElementConfig.setAlpha(getAlpha());
		return tWidgetConfig.viewElementConfig;
	}

	@Override
	public void setCustomizeModusListener(OnTouchListener customizeModusListener){
		tCustomizeModusListener = customizeModusListener;
	}

	@Override
	public void setProtocolMap(Map<String, String> protocolMap) {
		tWidgetConfig.protocolMap = protocolMap;
		updateProtocolMap();
	}

	@Override
	public List<UiInputData> getUiInputSourceList() {
		List<UiInputData> list = new ArrayList<UiInputData>();
		list.add(tChannelH);
		list.add(tChannelV);
	    return list;
	}
	
	public static ViewElementConfig getDefaultViewElementConfig() {
		return DefaultViewElementConfigRepo.getDefaultConfig(AnalogStickWidget.class);
	}

	@Override
	public void onNotifyUiOutputSink(Object p) {
		ConnectionState data = (ConnectionState)p;
		tIsControlling = ((s.TRY_CONN == data.getState()) || (s.CONN_CONTROL == data.getState()));
		postInvalidate();
	}

	@Override
	public UiOutputDataType getType(){
		return UiOutputDataType.ConnectionState;
	}
}
