/**
 * Rocket Colibri © 2014
 */
package ch.hsr.rocketcolibri;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import ch.hsr.rocketcolibri.activity.IUiInputSourceProtocolSelector;
import ch.hsr.rocketcolibri.db.RocketColibriDB;
import ch.hsr.rocketcolibri.db.RocketColibriDataHandler;
import ch.hsr.rocketcolibri.protocol.RCProtocol;
import ch.hsr.rocketcolibri.protocol.RCProtocolInterface;
import ch.hsr.rocketcolibri.protocol.RCProtocolUdp;
import ch.hsr.rocketcolibri.protocol.WifiConnection;
import ch.hsr.rocketcolibri.view.custimizable.ICustomizableView;
import ch.hsr.rocketcolibri.view.custimizable.ViewElementConfig;
import ch.hsr.rocketcolibri.view.widget.BluetoothConnectionStatusWidget;
import ch.hsr.rocketcolibri.view.widget.ConnectedUserInfoWidget;
import ch.hsr.rocketcolibri.view.widget.ConnectionStatusWidget;
import ch.hsr.rocketcolibri.view.widget.DefaultViewElementConfigRepo;
import ch.hsr.rocketcolibri.view.widget.AnalogStickWidget;
import ch.hsr.rocketcolibri.view.widget.HobbyKingWifiReceiverConnectionWidget;
import ch.hsr.rocketcolibri.view.widget.MotionControlWidget;
import ch.hsr.rocketcolibri.view.widget.RotaryKnobWidget;
import ch.hsr.rocketcolibri.view.widget.SerialConnectionWidget;
import ch.hsr.rocketcolibri.view.widget.SwitchWidget;
import ch.hsr.rocketcolibri.view.widget.VideoStreamWidget;
import ch.hsr.rocketcolibri.widgetdirectory.WidgetEntry;

/**
 * @short Service with all components that must be available during the entire App life cycle
 * 
 * Responsibilities:
 * - Provide a binder for the activities
 * - holds protocol and wifi connection objects
 * - holds DBService object
 */
public class RocketColibriService extends Service  {
	final String TAG = this.getClass().getName();
	
	public static volatile boolean tRunning;
	private final IBinder tBinder = new RocketColibriServiceBinder();

	// GUI Widget collection
	List <WidgetEntry> tWidgetDirectory= new ArrayList<WidgetEntry>();

	private RCProtocol tProtocol;
	public RCProtocolInterface tProtocolInterface;

	private RocketColibriDB tRocketColibriDB;

	@Override
	public IBinder onBind(Intent intent) {
		return tBinder;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "RocketColibriService started");
		RocketColibriService.tRunning = true;
		DefaultViewElementConfigRepo.getInstance(this);
		
		
		tProtocolInterface = new RCProtocolInterface() ;
		
		
		// list all available Widgets here: 
		this.tWidgetDirectory.add(new WidgetEntry("Analog Stick", AnalogStickWidget.class.getName(), AnalogStickWidget.getDefaultViewElementConfig()));
		this.tWidgetDirectory.add(new WidgetEntry("User Info", ConnectedUserInfoWidget.class.getName(), ConnectedUserInfoWidget.getDefaultViewElementConfig()));
		this.tWidgetDirectory.add(new WidgetEntry("Video Stream", VideoStreamWidget.class.getName(), VideoStreamWidget.getDefaultViewElementConfig()));
		this.tWidgetDirectory.add(new WidgetEntry("Switch", SwitchWidget.class.getName(), SwitchWidget.getDefaultViewElementConfig()));
		this.tWidgetDirectory.add(new WidgetEntry("Rotary knob", RotaryKnobWidget.class.getName(), RotaryKnobWidget.getDefaultViewElementConfig()));
		this.tWidgetDirectory.add(new WidgetEntry("Gravity", MotionControlWidget.class.getName(), MotionControlWidget.getDefaultViewElementConfig()));
		this.tWidgetDirectory.add(new WidgetEntry("RocketColibri Wifi", ConnectionStatusWidget.class.getName(), ConnectionStatusWidget.getDefaultViewElementConfig()));
		this.tWidgetDirectory.add(new WidgetEntry("RocketColibri Bluetooth", BluetoothConnectionStatusWidget.class.getName(), BluetoothConnectionStatusWidget.getDefaultViewElementConfig()));
		this.tWidgetDirectory.add(new WidgetEntry("HobbyKing Wifi Receiver", HobbyKingWifiReceiverConnectionWidget.class.getName(), HobbyKingWifiReceiverConnectionWidget.getDefaultViewElementConfig()));
		this.tWidgetDirectory.add(new WidgetEntry("USB Serial (Spektrum DSMX)", SerialConnectionWidget.class.getName(), SerialConnectionWidget.getDefaultViewElementConfig()));
		
		// create database instance
		tRocketColibriDB = new RocketColibriDB(this);
		try {
			//read rc.db and update the users client db
			new RocketColibriDataHandler(this, tRocketColibriDB);
		} catch (Exception e) {
			e.printStackTrace();
		}
   }
	 
    @Override
    public void onDestroy() {
        super.onDestroy();
        
        // The activity is about to be destroyed.       
        tRocketColibriDB.close();
        tRocketColibriDB = null;
        
        tProtocolInterface.stopNotifiyUiOutputData();
        tProtocolInterface.cancelOldCommandJob();
        
        tRunning = false;
    }

    @Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return Service.START_STICKY;
	}
    
    public RocketColibriDB getRocketColibriDB() {
    	return tRocketColibriDB;
    }
    


	/**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class RocketColibriServiceBinder extends Binder {
    	public RocketColibriService getService() {
            // Return this instance of RocketColibriProtocol so clients can call public methods
            return RocketColibriService.this;
        }
    }
	
	public List<WidgetEntry> getWidgetEntries(){
		return tWidgetDirectory;
	}
	/**
	 * physical connection established (e.g. Wifi connected)
	 */
	public void eventConnectionEstablished() {
		tProtocolInterface.startNotifiyUiOutputData();
		if(null != tProtocol)	
			if (!tProtocol.getNetworkId().equals(""))
				tProtocol.eventConnectionEstablished();
	}
	
	/**
	 * physical connection interrupted
	 */
	public void eventConnectionInterrupted() {
		tProtocolInterface.stopNotifiyUiOutputData();
		if(null != tProtocol)
			if (!tProtocol.getNetworkId().equals(""))
				tProtocol.eventConnectionInterrupted();
	}
	
	/**
	 * User input: start control
	 * @return true if the event is processed
	 */
	public boolean eventUserInput(){
		if(null != tProtocol)
			return tProtocol.eventUserInput();
		else
			return false;
	}
	public boolean eventUserDisconnectLayer1(){
		if(null != tProtocol)
			return tProtocol.eventUserInputDisconnectLayer1(this);
		else
			return false;
	}
	public boolean eventUserConnectLayer1(){
		if(null != tProtocol)
			return tProtocol.eventUserInputConnectLayer1(this);
		else
			return false;
	}
	/**
	 * creates the tProtocol object
	 * @param protocolSelectorObject
	 * @return
	 */
	public boolean protocolFactory(IUiInputSourceProtocolSelector protocolSelectorObject) throws Exception
	{
		if(null != tProtocol)
		{
			tProtocol.removeInterface();
		}
	    Class<?> c = Class.forName("ch.hsr.rocketcolibri.protocol."+ protocolSelectorObject.getProtocolName());
	    Constructor<?> cons = c.getConstructor(Context.class, RCProtocolInterface.class);
	    tProtocol = (RCProtocol)cons.newInstance(this, this.tProtocolInterface);   
	    return true;
	}
	
	public String getNetworkId()
	{
		if (null == tProtocol)
			return "";
		else
			return tProtocol.getNetworkId();		
	}
}
