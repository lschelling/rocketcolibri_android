package ch.hsr.rocketcolibri.protocol;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import jp.ksksue.driver.serial.FTDriver;
import ch.hsr.rocketcolibri.fsm.Action;
import ch.hsr.rocketcolibri.protocol.RocketColibriProtocolFsm.s;
import android.content.Context;
import android.hardware.usb.UsbManager;
import android.util.Log;

public class RCProtocolSerial extends RCProtocol {
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	static final String TAG = RCProtocolSerial.class.getName();
	private Future<?> executorFuture=null;
	public SimpleConnectionFsm tFsm;
	FTDriver tUsbSerial;
	
	Action<SimpleConnectionFsm> startSendingChannelData = new Action<SimpleConnectionFsm>() {
		public void apply(SimpleConnectionFsm fsm, Object event,
				Object nextState) 
		{ 
			Log.d(TAG, "execute action startSendChannelMessage");
			startSendingChannelData();
		}
	};
	
	Action<SimpleConnectionFsm> stopSendingChannelData = new Action<SimpleConnectionFsm>() {
		public void apply(SimpleConnectionFsm fsm, Object event,
				Object nextState) 
		{
			Log.d(TAG, "execute action startSendChannelMessage");
			cancelOldCommandJob();
		}
	};
	Action<SimpleConnectionFsm> updateState = new Action<SimpleConnectionFsm>() {
		public void apply(SimpleConnectionFsm fsm, Object event,	Object nextState) 
		{
			Log.d(TAG, "execute action updateState" + nextState);
			SimpleConnectionFsm.s state = (SimpleConnectionFsm.s)nextState;
			switch (state)
			{
			default:
			case DISC:
				tProtcolInterface.tConnState.setState(RocketColibriProtocolFsm.s.DISC);
				break;
			case CONN_CONTROL:
				tProtcolInterface.tConnState.setState(RocketColibriProtocolFsm.s.CONN_CONTROL);
				break;
			case CONN_OBSERVE:
				tProtcolInterface.tConnState.setState(RocketColibriProtocolFsm.s.CONN_OBSERVE);
				break;
			}
		}
	};

	@SuppressWarnings("static-access")
	public RCProtocolSerial( Context context, RCProtocolInterface protoIf) 
	{
		this.tProtcolInterface = protoIf;
		this.tProtcolInterface.tProtcolConfig.setAuto(false);
		tProtcolInterface.tConnState.setState(s.DISC);
		tUsbSerial = new FTDriver((UsbManager)context.getSystemService(context.USB_SERVICE));
		this.tFsm = new SimpleConnectionFsm(SimpleConnectionFsm.s.DISC);
		this.tProtcolInterface = protoIf;
		this.tProtcolInterface.tProtcolConfig.setAuto(true);
		tProtcolInterface.tConnState.setState(RocketColibriProtocolFsm.s.DISC);
		// initialize the actions of the state machine
		this.tFsm.getStateMachinePlan().entryAction(SimpleConnectionFsm.s.CONN_CONTROL, startSendingChannelData);
		this.tFsm.getStateMachinePlan().entryAction(SimpleConnectionFsm.s.DISC, stopSendingChannelData);
		this.tFsm.getStateMachinePlan().entryAction(SimpleConnectionFsm.s.CONN_OBSERVE, stopSendingChannelData);
		this.tFsm.getStateMachinePlan().leaveAction(SimpleConnectionFsm.s.DISC, updateState); 
		this.tFsm.getStateMachinePlan().leaveAction(SimpleConnectionFsm.s.CONN_OBSERVE, updateState); 
		this.tFsm.getStateMachinePlan().leaveAction(SimpleConnectionFsm.s.CONN_CONTROL,  updateState);
	}
	
	@Override
	public void eventConnectionEstablished() {
		tFsm.queue(SimpleConnectionFsm.e.E1_CONN_LAYER1);
		tFsm.processOutstandingEvents();
		Log.d(TAG, "eventConnectionEstablished");
	}
	
	/**
	 * physical connection interrupted
	 */
	@Override
	public void eventConnectionInterrupted() {
		cancelOldCommandJob();
		tFsm.queue(SimpleConnectionFsm.e.E2_DISC_LAYER1);
		tFsm.processOutstandingEvents();
		Log.d(TAG, "eventConnectionEstablished");
	}
	
	
	/**
	 * User input stop control
	 * @return true if the event is processed
	 */
	@Override
	public boolean eventUserInput(){
		tFsm.queue(SimpleConnectionFsm.e.E4_USR_CLICK);
		tFsm.processOutstandingEvents();
		Log.d(TAG, "eventUserInput");
		return true;
	}
	
	@Override
	public boolean eventUserInputDisconnectLayer1(Context context){
		tUsbSerial.end();
		eventConnectionInterrupted();
		return tUsbSerial.isConnected();
	}
	
	public boolean eventUserInputConnectLayer1(Context context){	
		tUsbSerial.begin(112500);
		eventConnectionEstablished();
		return tUsbSerial.isConnected();
	}

	
	void startSendingChannelData() {
		cancelOldCommandJob();
		final Runnable every11ms = new Runnable() 
		{
			int dbgCnt;
			public void run() 
			{
				
				ProtocolHelper protocol = new ProtocolHelper(tProtcolInterface.tChannelList);
				if (tUsbSerial.isConnected())
				{
				    tUsbSerial.write(protocol.getSpektrumDSMData());
				}
				dbgCnt++;
				if(0==(dbgCnt % 100))
				Log.d(TAG, protocol.getHexStringFromBuffer(protocol.getSpektrumDSMData()));
			}
		};
		if(null != this.executorFuture)
			this.executorFuture.cancel(true);
		this.executorFuture = scheduler.scheduleAtFixedRate(every11ms, 0, 15, TimeUnit.MILLISECONDS);
	}
	
	/**
	*  cancel the running command Executor
	*/
	public void cancelOldCommandJob()
	{
		if(null != this.executorFuture)
		{
			this.executorFuture.cancel(true);
			this.executorFuture=null;
		}		
	}
}

