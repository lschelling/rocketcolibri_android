package ch.hsr.rocketcolibri.protocol;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.util.Log;
import ch.hsr.rocketcolibri.RocketColibriService;
import ch.hsr.rocketcolibri.fsm.Action;
import ch.hsr.rocketcolibri.protocol.SimpleConnectionFsm.s;
import ch.hsr.rocketcolibri.ui_data.input.IUiInputDataObserver;
import ch.hsr.rocketcolibri.ui_data.input.UiInputSourceChannel;

/**
 * This is the implementation of the HK Wifi Receiver protocol.
 * 
 * http://www.ezc-rc.cn/
 * http://www.hobbyking.com/hobbyking/store/__21430__Hobbyking_IOS_Android_4CH_WiFi_Receiver.html
 * 
 * To setup the the connection to the Wifi Receiver module, change to customize mode and to select the HK widget.
 * 
 * Edit the protocol parameter with the WPA SSID (something like 'Ezc  Wifi 035' take care of the spaces, there 
 * may be more than one space char in the SSID! ) and the password (55555555 in my case) provided with the receiver.
 * 
 * There is not much information available about the protocol, but it seems to be very simple.
 * The HK Wifi Reciever unit waits on IP 10.10.100.254 on port 8899. The socket timeout should be set to 1000ms.
 * The message is 11 bytes. The first 4 bytes are [85, 0, 11, 0]. The values of the 4 channels are transmitted in 
 * order(channels 1-4). 
 * The next 2 bytes are not used at all.  The sum of bytes 1-10 are transmitted as last byte.
 */
public class HobbyKingWifiReceiverProtocol extends RCProtocol implements  IUiInputDataObserver
{
	static final String TAG = HobbyKingWifiReceiverProtocol.class.getName();
	final Semaphore channelChangeSema = new Semaphore(1, true);
	public WifiConnection tWifi;
	private Thread tSendThread;
	private boolean sendThreadIsRunning;
	Socket tClient;
	SimpleConnectionFsm tFsm;
	DataOutputStream tDataOut;
	
	private void registerChannelObserver()
	{
		for(UiInputSourceChannel c :tProtcolInterface.tChannelList) 
		{
			c.registerChannelObserver(this);
		}
	}
		
	Action<SimpleConnectionFsm> startSendingChannelData = new Action<SimpleConnectionFsm>() {
		public void apply(SimpleConnectionFsm fsm, Object event,
				Object nextState) 
		{
			tSendThread = new Thread(
					new Runnable()
					{
						public void run()
						{
							registerChannelObserver();
							InitSocket();
							sendThreadIsRunning = true;
							while(sendThreadIsRunning)
							{
								Log.d(TAG, "running");
								try 
								{
									Thread.sleep(5);
									channelChangeSema.drainPermits();
									if(!channelChangeSema.tryAcquire(1000, TimeUnit.MILLISECONDS))
									{
										Log.d(TAG, "update without user input");
										channelChangeSema.release(0);
									}						
									// Create an empty ByteBuffer with a 10 byte capacity
								    byte[] bbuf = new byte[11];
								    bbuf[0]=85; // position=0
								    bbuf[1]=0; // position=1
								    bbuf[2]=11; // position=2
								    bbuf[3]=0; // position=3
								    bbuf[4]=0; // position=4 ch1
								    bbuf[5]=0; // position=5 ch2
								    bbuf[6]=0; // position=6 ch3
								    bbuf[6]=0; // position=7 ch4	
									for(UiInputSourceChannel c :tProtcolInterface.tChannelList) {
										if(c.getChannelAssignment() <= 4 && 
										   c.getChannelAssignment() > 0 && 
										   c.getChannelValue() >= 0 &&
										   c.getChannelValue() <= 1000) {
										    bbuf[4+c.getChannelAssignment()-1] =((byte)(Math.min(255,c.getChannelValue()/10)));
										}
									}
									bbuf[8]=0; // position=8
									bbuf[9]=0; // position=9
									int sum=0;
									for(int i=0; i<10; i++)
										sum += bbuf[i]; 
									bbuf[10]=(byte)sum; // position=10
									try {
										tDataOut.write(bbuf, 0, 11);
		//									Log.d(TAG, "write msg:"+(int)(bbuf[0]&0xFF)+":"+(int)(bbuf[1]&0xFF)+":"+(int)(bbuf[2]&0xFF)+
		//											            ":"+(int)(bbuf[3]&0xFF)+":"+(int)(bbuf[4]&0xFF)+":"+(int)(bbuf[5]&0xFF)+
		//											            ":"+(int)(bbuf[6]&0xFF)+":"+(int)(bbuf[7]&0xFF)+":"+(int)(bbuf[8]&0xFF)+
		//											            ":"+(int)(bbuf[9]&0xFF)+":"+(int)(bbuf[10]&0xFF));
									} catch (IOException e) {
										Log.d(TAG, "error wirting to socket");
									}
								} catch (InterruptedException e1) {
									Log.d(TAG, "thread interrupted");
								}
							}
						}
					});
			tSendThread.start();
			Log.d(TAG, "execute action startSendChannelMessage");
			
		}
	};
	
	Action<SimpleConnectionFsm> stopSendingChannelData = new Action<SimpleConnectionFsm>() {
		public void apply(SimpleConnectionFsm fsm, Object event,
				Object nextState) 
		{
			Log.d(TAG, "execute action startSendChannelMessage");
			cancelOldCommandJob();
			CloseSocket();
		}
	};

	Action<SimpleConnectionFsm> updateState = new Action<SimpleConnectionFsm>() {
		public void apply(SimpleConnectionFsm fsm, Object event,	Object nextState) 
		{
			Log.d(TAG, "execute action updateState" + nextState);
			SimpleConnectionFsm.s state = (SimpleConnectionFsm.s)nextState;
			if(tProtcolInterface.tConnState != null)
			{
				switch (state)
				{
				default:
				case DISC:
					tProtcolInterface.tConnState.setState(RocketColibriProtocolFsm.s.DISC);
					break;
				case CONN_CONTROL:
					tProtcolInterface.tConnState.setState(RocketColibriProtocolFsm.s.CONN_CONTROL);
					break;
				case CONN_OBSERVE:
					tProtcolInterface.tConnState.setState(RocketColibriProtocolFsm.s.CONN_OBSERVE);
					break;
				}
			}
			else
			{
				Log.d(TAG, "tConnState is null");
			}
		}
	};

	public HobbyKingWifiReceiverProtocol( Context context, RCProtocolInterface protoIf) 
	{
		this.tWifi = new WifiConnection();
		this.tFsm = new SimpleConnectionFsm(s.DISC);
		this.tProtcolInterface = protoIf;
		this.tProtcolInterface.tProtcolConfig.setAuto(true);
		tProtcolInterface.tConnState.setState(RocketColibriProtocolFsm.s.DISC);
		// initialize the actions of the state machine
		this.tFsm.getStateMachinePlan().entryAction(s.CONN_CONTROL, startSendingChannelData);
		this.tFsm.getStateMachinePlan().entryAction(s.DISC, stopSendingChannelData);
		this.tFsm.getStateMachinePlan().entryAction(s.CONN_OBSERVE, stopSendingChannelData);
		this.tFsm.getStateMachinePlan().leaveAction(s.DISC, updateState); 
		this.tFsm.getStateMachinePlan().leaveAction(s.CONN_OBSERVE, updateState); 
		this.tFsm.getStateMachinePlan().leaveAction(s.CONN_CONTROL,  updateState);
	}
	
	/**
	 * opens a TCP socket for the communication with the HobbyKing receiver
	 */
	private void InitSocket() 
	{
		try 
		{
		    tClient = new Socket("10.10.100.254", 8899);
		    tClient.setSoTimeout(1000);
		    tDataOut = new DataOutputStream(tClient.getOutputStream());
		    Log.d(TAG, "open client");
		} catch (IOException e1) 
		{
			e1.printStackTrace();
	        Log.d(TAG, "Failed to resolve ip address due to UnknownException: " + e1.getMessage() );       
		}
	}
	
	private void CloseSocket()
	{
		try {
			if(null != tClient)
				tClient.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * physical connection interrupted
	 */
	@Override
	public void eventConnectionInterrupted() {
		cancelOldCommandJob();
		tFsm.queue(SimpleConnectionFsm.e.E2_DISC_LAYER1);
		tFsm.processOutstandingEvents();
		Log.d(TAG, "eventConnectionEstablished");
	}
	@Override
	public void eventConnectionEstablished() {
		tFsm.queue(SimpleConnectionFsm.e.E1_CONN_LAYER1);
		tFsm.processOutstandingEvents();
		Log.d(TAG, "eventConnectionEstablished");
	}
	
	/**
	 * User input stop control
	 * @return true if the event is processed
	 */	
	@Override
	public boolean eventUserInput(){
		tFsm.queue(SimpleConnectionFsm.e.E4_USR_CLICK);
		tFsm.processOutstandingEvents();
		Log.d(TAG, "eventUserInput");
		return true;
	}
	
	/**
	 * User input: start control
	 * @return true if the event is processed
	 */
	@Override
	public boolean eventUserInputDisconnectLayer1(Context context){
		tWifi.disconnectRocketColibriSSID(context);
		return true;
	}

	/**
	 * User input: start control
	 * @return true if the event is processed
	 */
	@Override
	public boolean eventUserInputConnectLayer1(Context context){
		tWifi.connectRocketColibriSSID((RocketColibriService)context);
		return true;
	}
	
	/**
	*  cancel the running command Executor
	*/
	public void cancelOldCommandJob()
	{
		sendThreadIsRunning=false;

	}
	
	public String getNetworkId()
	{
		return tWifi.networkSSID;
	}

	/**
	 * this function is called whenever a channel has changed its value
	 */
	@Override
	public void onNotifyUiInputSink(Object data) {
		channelChangeSema.release();
	}
}
