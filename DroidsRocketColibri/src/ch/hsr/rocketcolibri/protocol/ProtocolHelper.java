package ch.hsr.rocketcolibri.protocol;

import java.util.List;

import android.util.Log;
import ch.hsr.rocketcolibri.ui_data.input.UiInputSourceChannel;

public class ProtocolHelper {
	
	static final int MAX_CH_RANGE_ROCKETCOLIBRI = 1000;
	static final int MAX_CH_RANGE_SPEKTRUM_DSM2 = 1024;
	
	int[] channels = {0,0,0,0,0,0,0,0 }; 

	public ProtocolHelper(List<UiInputSourceChannel> channelList)
	{
		// map channel list to the channel array
		for(UiInputSourceChannel c :channelList) 
		{
			if(c.getChannelAssignment() <= 8 && 
			   c.getChannelAssignment() > 0 && 
			   c.getChannelValue() >= 0 &&
			   c.getChannelValue() <= 1000) 
			{
				// multiple UI input sources can be assigned to one channel 
				channels[c.getChannelAssignment()-1] += c.getChannelValue(); 

				// limit channel range is 1000
				if(channels[c.getChannelAssignment()-1] > 1000) 
					channels[c.getChannelAssignment()-1]=1000;
			}
		}
	}

	
	public int scaleRange(int maxRange, int channelValue)
	{
		return channelValue * maxRange / MAX_CH_RANGE_ROCKETCOLIBRI;
	}
	
	/** 
	 * converts the binary buffer to a hex string
	 * @param bbuf
	 * @return hex string
	 */
	public String getHexStringFromBuffer(byte[] bbuf)
	{
		final StringBuilder builder = new StringBuilder();
		for(byte b : bbuf)
			builder.append(String.format("%02x ", b));
		return builder.toString();
	}
	
	/**
	 * @note this function has been tested together with the OpenLrsNg implementation of the 
	 *       Spektrum protocol. (and not with original Spektrum Equipment)
	 * 
	 * This documents contains the informations I found about the Spektrum protocol
	 * @see https://pixhawk.org/_media/dev/dsm2_dsm_x_protocol.pdf
	 * 
	 * @return byte buffer with the DSM2/DSMX data frame
	 */
	public byte[] getSpektrumDSMData()
	{
		byte[] bbuf = { 0,0,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,
		        (byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF };

		// first 2 bytes are 0
		for (int ch=0; ch <8; ch++)
		{
			int channel = 0;
			if(channels[ch] != 0)
			{
				channel = scaleRange(MAX_CH_RANGE_SPEKTRUM_DSM2, channels[ch])-1;
				bbuf[2+ch*2]=(byte)(ch+1<<3);				// insert channel ID at bit pos 11-14
				bbuf[2+ch*2] |= (byte)(channel >> 8);	// insert channel value at bit pos 0;
				bbuf[2+ch*2+1] = (byte)(channel & 0xFF);
			}
		}
		return bbuf;
	}
}
