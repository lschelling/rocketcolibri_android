/**
 * Rocket Colibri © 2014
 */
package ch.hsr.rocketcolibri.protocol;

import ch.hsr.rocketcolibri.fsm.StateMachine;
import ch.hsr.rocketcolibri.fsm.StateMachinePlan;

/**
 * Implementation of the state RocketColibri state machine
 * 
 */
public class SimpleConnectionFsm extends StateMachine {
	/**
	 * States
	 */
	public enum s {
		DISC, CONN_OBSERVE, CONN_CONTROL
	}

	/**
	 * Events
	 */
	public enum e {
		E1_CONN_LAYER1, E2_DISC_LAYER1, E3_DISC_SOCKET, E4_USR_CLICK
	};

	/**
	 * Static so that it's only constructed once on class load
	 * You obviously shouldn't store individual state in the plan.
	 * Your FSM will be passed into the transition Actions
	 */
	private static final StateMachinePlan sPLAN = new StateMachinePlan() {
		{
// @formatter:off
ri(null,			e.E1_CONN_LAYER1,	e.E2_DISC_LAYER1, 	e.E3_DISC_SOCKET, 	e.E4_USR_CLICK);
at(s.DISC, 			s.CONN_OBSERVE,		s.DISC,				s.DISC, 			s.DISC);
at(s.CONN_OBSERVE, 	s.CONN_OBSERVE,		s.DISC,				s.CONN_CONTROL, 	s.CONN_CONTROL);
at(s.CONN_CONTROL,	s.CONN_CONTROL, 	s.DISC,				s.CONN_CONTROL, 	s.CONN_OBSERVE);
// @formatter:on
		}
	};

	public SimpleConnectionFsm(Object aStartState) {
		super(sPLAN, aStartState);
	}
}