package ch.hsr.rocketcolibri.protocol;

import android.content.Context;

public class RCProtocol{
	public RCProtocolInterface tProtcolInterface;
	
	public RCProtocol() {
		this.tProtcolInterface = null;
	}

	public RCProtocol(RCProtocolInterface protoIf) {
		this.tProtcolInterface = protoIf;
	}
	
	public void addInterface(RCProtocolInterface protoIf) {
		this.tProtcolInterface = protoIf;
	}
	
	public void removeInterface() {
		this.tProtcolInterface = null;
	}
	public void eventConnectionEstablished() {
	}
	
	/**
	 * physical connection interrupted
	 */
	
	public void eventConnectionInterrupted() {
	}
	
	
	/**
	 * User input start/stop control
	 * @return true if the event is processed
	 */
	public boolean eventUserInput(){
		return true;
	}
	
	/**
	 * User input start/stop control
	 * @return true if the event is processed
	 */
	public boolean eventUserInputDisconnectLayer1(Context context){
		return true;
	}
	public boolean eventUserInputConnectLayer1(Context context){
		return true;
	}

	public String getNetworkId()
	{
		return "";
	}
}
