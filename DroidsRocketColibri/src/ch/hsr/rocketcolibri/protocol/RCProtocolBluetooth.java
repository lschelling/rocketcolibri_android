package ch.hsr.rocketcolibri.protocol;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import ch.hsr.rocketcolibri.fsm.Action;
import ch.hsr.rocketcolibri.protocol.SimpleConnectionFsm.s;
import ch.hsr.rocketcolibri.protocol.bluetooth.BtSerial;
import ch.hsr.rocketcolibri.ui_data.input.UiInputSourceChannel;
import android.content.Context;
import android.util.Log;

public class RCProtocolBluetooth extends RCProtocol {
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	static final String TAG = RCProtocolBluetooth.class.getName();
	private Future<?> executorFuture=null;
	public SimpleConnectionFsm tFsm;
	BtSerial tBtSerial;
	
	Action<SimpleConnectionFsm> startSendingChannelData = new Action<SimpleConnectionFsm>() {
		public void apply(SimpleConnectionFsm fsm, Object event,
				Object nextState) 
		{ 
			Log.d(TAG, "execute action startSendChannelMessage");
			startSendingChannelData();
		}
	};
	
	Action<SimpleConnectionFsm> stopSendingChannelData = new Action<SimpleConnectionFsm>() {
		public void apply(SimpleConnectionFsm fsm, Object event,
				Object nextState) 
		{
			Log.d(TAG, "execute action startSendChannelMessage");
			cancelOldCommandJob();
		}
	};

	public RCProtocolBluetooth( Context context, RCProtocolInterface protoIf) 
	{
		this.tProtcolInterface = protoIf;
		this.tProtcolInterface.tProtcolConfig.setAuto(false);
		
		tBtSerial = new BtSerial(context);
		this.tFsm = new SimpleConnectionFsm(s.DISC);
		this.tProtcolInterface = protoIf;
		this.tProtcolInterface.tProtcolConfig.setAuto(true);
		
		// initialize the actions of the state machine
		this.tFsm.getStateMachinePlan().entryAction(s.CONN_CONTROL, startSendingChannelData);
		this.tFsm.getStateMachinePlan().entryAction(s.DISC, stopSendingChannelData);
		this.tFsm.getStateMachinePlan().entryAction(s.CONN_OBSERVE, stopSendingChannelData);
	}
	
	@Override
	public void eventConnectionEstablished() {
		tFsm.queue(SimpleConnectionFsm.e.E1_CONN_LAYER1);
		tFsm.processOutstandingEvents();
		Log.d(TAG, "eventConnectionEstablished");
	}
	
	/**
	 * physical connection interrupted
	 */
	@Override
	public void eventConnectionInterrupted() {
		cancelOldCommandJob();
		tFsm.queue(SimpleConnectionFsm.e.E2_DISC_LAYER1);
		tFsm.processOutstandingEvents();
		Log.d(TAG, "eventConnectionEstablished");
	}
	
	
	/**
	 * User input stop control
	 * @return true if the event is processed
	 */
	@Override
	public boolean eventUserInput(){
		tFsm.queue(SimpleConnectionFsm.e.E4_USR_CLICK);
		tFsm.processOutstandingEvents();
		Log.d(TAG, "eventUserInput");
		return true;
	}
	
	@Override
	public boolean eventUserInputDisconnectLayer1(Context context){
		tBtSerial.disconnect();
		eventConnectionInterrupted();
		return tBtSerial.isConnected();
	}
	
	public boolean eventUserInputConnectLayer1(Context context){
		String[] btDevicesMac = tBtSerial.list(false);
		String[] btDevices = tBtSerial.list(true);
		for(int i=0; i<btDevices.length; i++)
		{
			if(btDevices[i].contains(this.tProtcolInterface.tProtcolConfig.getBluetoothDevice())){
				Log.d(TAG, "found device" + i + btDevices[i] );
				boolean retval = tBtSerial.connect(btDevicesMac[i]);
				if (!retval)
				{
					Log.d(TAG, "connection to" + btDevices[i] + " failed" );
				}
				else
					eventConnectionEstablished();
				return retval;		
			}
		}
		Log.d(TAG, "device not found!");
		return tBtSerial.isConnected();
	}

	
	void startSendingChannelData() {
		cancelOldCommandJob();
		final Runnable every20ms = new Runnable() 
		{
			public void run() 
			{
				if (tBtSerial.isConnected())
				{
					
					for(UiInputSourceChannel c :tProtcolInterface.tChannelList) {
						if(c.getChannelAssignment() <= 8 && 
						   c.getChannelAssignment() > 0 && 
						   c.getChannelValue() >= 0 &&
						   c.getChannelValue() <= 1000) {
							// channel 1 is at position 0
							// multiple widgets can be assigned to one channel (thats why there is a '+=' )
							String cmd = c.getChannelAssignment() + "=" + c.getChannelValue() + "\n";
							Log.d("Bluetooth send:","channel:" + cmd);
							tBtSerial.write(cmd);
						}
					}
				}
				else
				{
					eventConnectionInterrupted();
				}
			}
		};
		if(null != this.executorFuture)
			this.executorFuture.cancel(true);
		this.executorFuture = scheduler.scheduleAtFixedRate(every20ms, 0, 50, TimeUnit.MILLISECONDS);
	}
	
	/**
	*  cancel the running command Executor
	*/
	public void cancelOldCommandJob()
	{
		if(null != this.executorFuture)
		{
			this.executorFuture.cancel(true);
			this.executorFuture=null;
		}		
	}
}

