package ch.hsr.rocketcolibri.activity;

public interface IUiInputSourceProtocolSelector {

	String getProtocolName();
}
