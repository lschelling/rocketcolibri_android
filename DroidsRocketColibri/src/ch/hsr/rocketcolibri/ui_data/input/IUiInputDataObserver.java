package ch.hsr.rocketcolibri.ui_data.input;

public interface IUiInputDataObserver {
	/**
	 * UI thread sends UiInputSink change notification with this methods
	 * The Object class depends on the return value of getType
	 * @param data data object
	 */
	void onNotifyUiInputSink(Object data);
}
