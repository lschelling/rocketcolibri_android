# RocketColibri remote control system #

The RocketColibri application allows you to control a RC model with standard components like ESC and servos from your Android phone.

The RocketColibri RC receivers:

1. [Wifi ServoController](https://bitbucket.org/lschelling/rocketcolibri_wifi_servocontroller), communicats over UDP (over Wifi connection). Runs on Linux embedded systems such as the Raspberry Pi, Linino or any embedded linux board with PWM outputs.
2. Spectrum DSM protocol over USB serial (FTDI converter and others)
3. [HobbyKing Wifi Receiver](http://www.hobbyking.com/hobbyking/store/__21430__Hobbyking_IOS_Android_4CH_WiFi_Receiver.html). This is a 4 channel Wifi receiver available from HobbyKing.
4. [Bluetooth ServoController](https://bitbucket.org/lschelling/rocketcolibri_bluetooth_servocontroller), communicates over bluetooth serial. Runs on any Arduino board with a serial Bluetooth module. PPM and PWM versions available.